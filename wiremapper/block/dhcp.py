from pockethernet import DhcpResult
from wiremapper.block.block import Block
import gi

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GLib, GObject, Gio, GdkPixbuf


class DhcpBlock(Block):
    def make(self, result):
        if not isinstance(result, DhcpResult):
            return

        listbox = Gtk.ListBox()
        listbox.get_style_context().add_class('content')

        grid = Gtk.Grid()
        grid.set_row_spacing(12)
        grid.set_column_spacing(12)

        grid.attach(self._dim_label("Address", xalign=1), 0, 0, 1, 1)
        grid.attach(self._dim_label("Netmask", xalign=1), 0, 1, 1, 1)
        grid.attach(self._dim_label("Gateway", xalign=1), 0, 2, 1, 1)
        grid.attach(self._dim_label("DNS", xalign=1), 0, 3, 1, 1)
        grid.attach(self._dim_label("Filename", xalign=1), 0, 4, 1, 1)
        grid.attach(self._dim_label("Server", xalign=1), 0, 5, 1, 1)

        grid.attach(Gtk.Label(label=str(result.your_ip), xalign=0), 1, 0, 1, 1)
        if result.subnet_mask is not None:
            grid.attach(Gtk.Label(label=str(result.subnet_mask), xalign=0), 1, 1, 1, 1)
        if result.gateway is not None:
            addrs = []
            for addr in result.gateway:
                addrs.append(str(addr))
            grid.attach(Gtk.Label(label=', '.join(addrs), xalign=0), 1, 2, 1, 1)
        if result.nameservers is not None:
            addrs = []
            for addr in result.nameservers:
                addrs.append(str(addr))
            grid.attach(Gtk.Label(label=', '.join(addrs), xalign=0), 1, 3, 1, 1)
        grid.attach(Gtk.Label(label=result.filename, xalign=0), 1, 4, 1, 1)
        grid.attach(Gtk.Label(label=result.next_server, xalign=0), 1, 5, 1, 1)
        self._add_simple_row(listbox, grid)

        grid2 = Gtk.Grid()
        grid2.set_row_spacing(12)
        grid2.set_column_spacing(12)

        grid2.attach(Gtk.Label("All DHCP options", xalign=0), 0, 0, 3, 1)

        i = 1
        for tag, name, value in result.options_list:
            grid2.attach(self._dim_label(str(tag), xalign=1), 0, i, 1, 1)
            grid2.attach(self._dim_label(str(name), xalign=1), 1, i, 1, 1)
            if isinstance(value, list):
                temp = []
                for item in value:
                    temp.append(str(item))
                value = ', '.join(temp)
            grid2.attach(Gtk.Label(str(value), xalign=0), 2, i, 1, 1)
            i += 1

        self._add_simple_row(listbox, grid2)

        listbox.set_selection_mode(Gtk.SelectionMode.NONE)
        return self._make_result("DHCP", listbox)
